import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ping } from '../models/ping';
import { User } from '../models/user';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PingService {

  apiURL = 'http://147.139.167.88:20001';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'User-Agent': 'PostmanRuntime/7.26.2',
      'Accept': '*/*',
      'Accept-Encoding': 'gzip, deflate, br',
      'Connection': 'keep-alive',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJUb2tlblVzZXJSZXNwb25zZShBdXRob3JpemF0aW9uPW51bGwsIFJvbGVzPW51bGwsIFByaXZpbGVnZXM9bnVsbCkiLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sImlhdCI6MTU5NjgxMTM0MiwiZXhwIjoxNTk2ODk3NzQyfQ.-Wz4IeJuBnqWbe59c9NZXPpigwELJJkK3YubnOOlt2RExClPKRZaIeNf45OV9zX-IJFVpYgAEHwBxRegC3YhxQ',
    })
  };

  httpOptionsPost = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      //'Access-Control-Allow-Origin': '*',
      //'Origin': '*',
    })
  };

  getPing(): Observable<Ping> {
    return this.http.get<Ping>(this.apiURL + '/ping', this.httpOptions )
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getGithub(): Observable<Ping> {
    //'https://api.github.com/users/adiputra17'
    return this.http.get<Ping>('http://147.139.167.88:20001/test' )
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  postAuth(user): Observable<User> {
    let userObj = { username: "garda-admin", password: "P@ssw0rd123"};
    //window.alert(userObj.username);
    return this.http.post<User>(this.apiURL + '/auth', JSON.stringify(userObj), this.httpOptionsPost)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //window.alert(errorMessage);
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
