 import { Injectable } from '@angular/core';
 import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
 import { Observable } from 'rxjs';
 import { AuthService } from './auth.service';
 import { Cookies } from '../configs/cookies';
 import { Constants } from '../configs/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  loginTime: any;
  expired: any;
  routeURL: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private cookies: Cookies,
  ) {
    this.routeURL = this.router.url;
  }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    this.getExpired();
    return new Promise((resolve, reject) => {
      console.log('routerURL', this.routeURL);

      let auth = this.cookies.get('Authorization');
      if (auth === '') {
        console.log('auth => ', auth);
        this.cookies.put('Authorization', null);
      }

      let auth2 = this.cookies.get('Authorization');
      if ((auth2 == null || auth2 == 'null') && (this.routeURL !== '/login' || this.routeURL === '/login')) {
        console.log('auth', auth);
        this.routeURL = '/login';
        this.router.navigate(['/login'], {
          queryParams: {
            return: '/login'
          },
        });
        if (this.routeURL == '/') {
          console.log('reload');
          window.location.reload();
          return resolve(false);
        }
      } else {
        this.routeURL = this.router.url;
        return resolve(true);
      }
    });
  }

  getExpired () {
    this.loginTime = this.cookies.get(Constants.LOGINTIME);
    var date = new Date(this.loginTime);
    date.setHours(date.getHours() + 24);
    this.expired = date;
    console.log('EXP::', this.expired);
    console.log('DATE::', new Date());
    if (new Date() > this.expired) {
      this.cookies.put(Constants.AUTHORIZATION, null);
      this.cookies.put(Constants.ROLES, null);
      this.cookies.put(Constants.LOGINTIME, null);
      this.router.navigate(['/login']);
    }
  }

}
