import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { catchError, retry } from 'rxjs/operators';
import { Api } from '../configs/api';
import { Headers } from '../configs/headers';
import { Cookies } from '../configs/cookies';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private api: Api,
    private headers: Headers,
    private cookies: Cookies
  ) { }

  login(user): Observable<User> {
    return this.http.post<User>(this.api.URL + '/auth', JSON.stringify(user), {
      headers: this.headers.getHeaderPost()
    })
      .pipe(
        retry(1),
      )
  }
}
