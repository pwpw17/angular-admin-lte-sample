import { CanActivate } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Api } from '../configs/api';
import { Headers } from '../configs/headers';
import { Cookies } from '../configs/cookies';
import { Observable } from 'rxjs';
import { Ping } from '../models/ping';
import { catchError, retry } from 'rxjs/operators';
import { GSocial } from '../models/g-social';
import { Constants } from '../configs/constants';
import { Params } from '../configs/params';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GSocialService {

  constructor(
    private http: HttpClient,
    private api: Api,
    public header: Headers,
    private cookies: Cookies,
    private params: Params,
  ) { }

  getAll(): Observable<GSocial> {
    return this.http.get<GSocial>(this.api.URL + '/gsocial/find', {
      headers: this.header.getHeader(this.cookies.get(Constants.AUTHORIZATION)),
      params: this.params.getParams('title', '', '', '', '')
    })
      .pipe(
        retry(1)
      )
  }

  insert(data: any): Observable<any> {
    const formData = new FormData();
    for ( var key in data ) {
      formData.append(key, data[key]);
    }
    console.log('FORMDATA::', formData);

    return this.http.post<any>(this.api.URL + '/gsocial/insert', formData, {
      headers: this.header.getHeader(this.cookies.get(Constants.AUTHORIZATION)),
    })
      .pipe(
        retry(1)
      )
  }

  update(id: any, data: any): Observable<any> {
    const formData = new FormData();
    for ( var key in data ) {
      formData.append(key, data[key]);
    }
    console.log('FORMDATA::', formData);

    return this.http.post<any>(this.api.URL + '/gsocial/update/' + id, formData, {
      headers: this.header.getHeader(this.cookies.get(Constants.AUTHORIZATION)),
    })
      .pipe(
        retry(1)
      )
  }

  view(id: number) {
    return this.http.get<GSocial>(this.api.URL + '/gsocial/findById/' + id,{
      headers: this.header.getHeader(this.cookies.get(Constants.AUTHORIZATION))
    })
      .pipe(
        retry(1)
      )
  }

  delete(id: number) {
    console.log('ID', id);
    return this.http.delete(this.api.URL + '/gsocial/deleteById/' + id,{
      headers: this.header.getHeader(this.cookies.get(Constants.AUTHORIZATION))
    })
      .pipe(
        retry(1)
      )
  }

  registration(id: number) {
    return this.http.get<any>(this.api.URL + '/gsocial/findRegisterUserDetailById/' + id,{
      headers: this.header.getHeader(this.cookies.get(Constants.AUTHORIZATION))
    })
      .pipe(
        retry(1)
      )
  }

}
