import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Api } from '../configs/api';
import { Headers } from '../configs/headers';
import { Cookies } from '../configs/cookies';
import { Params } from '../configs/params';
import { Observable } from 'rxjs';
import { Constants } from '../configs/constants';
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  constructor(
    private http: HttpClient,
    private api: Api,
    public header: Headers,
    private cookies: Cookies,
    private params: Params,
  ) { }

  getAll(): Observable<any> {
    return this.http.get<any>(this.api.URL + '/findCity', {
      headers: this.header.getHeader(this.cookies.get(Constants.AUTHORIZATION))
    })
      .pipe(
        retry(1)
      )
  }
}
