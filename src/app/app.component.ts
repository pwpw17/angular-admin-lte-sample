import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'angular-admin-lte';
import { Cookies } from './configs/cookies';
import { Constants } from './configs/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  public customLayout: boolean;

  constructor(
    private layoutService: LayoutService,
    private cookies: Cookies,
  ) {}

  ngOnInit() {
    this.layoutService.isCustomLayout.subscribe((value: boolean) => {
      this.customLayout = value;
    });
  }
}
