export class GSocial {
  id: number;
  createdAt: Date;
  createdBy: string;
  updatedAt: Date;
  updatedBy: string;
  title: string;
  dateStart: Date;
  dateEnd: Date;
  description: string;
  city: string;
  location: string;
  termsCondition: string;
  imageBannerName: string;
  quota: number;
  joinLimit: number;
  image: number;
}
