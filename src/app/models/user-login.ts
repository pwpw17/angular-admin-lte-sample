export class UserLogin {
  mobile_phone: string;
  alamat: string;
  anggota_komunitas: string;
  birth_date: Date;
  create_datetime: string;
  full_name: string;
  gender: string;
  id_number: string;
  id_ojol: string;
  jenis_kendaraan: string;
  jumlah_anak: string;
  kecamatan: string;
  kota: string;
  merk_kendaraan: string;
  nopol: string;
  provinsi: string;
  status_perkawinan: string;
  tahun_pembuatan: string;
  tipe_kendaraan: string;
  update_datetime: string;
}
