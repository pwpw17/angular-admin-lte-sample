import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { GSocialComponent } from './components/g-social/g-social.component';
import { GSocialFormComponent } from './components/g-social/g-social-form/g-social-form.component';
import { GUmkmComponent } from './components/g-umkm/g-umkm.component';
import { GBeasiswaComponent } from './components/g-beasiswa/g-beasiswa.component';
import { GInsuranceComponent } from './components/g-insurance/g-insurance.component';
import { GNewsComponent } from './components/g-news/g-news.component';
import { GTrainingComponent } from './components/g-training/g-training.component';
import { GVoucherComponent } from './components/g-voucher/g-voucher.component';

import { BrowserModule } from '@angular/platform-browser';
import { AuthGuardService } from './services/auth-guard.service';
import { GSocialViewComponent } from './components/g-social/g-social-view/g-social-view.component';

const routes: Routes = [
  {
    path: '',
    data: { title: 'Garda Digital Admin' },
    children: [
      {
        path: '',
        component: HomeComponent,
        canActivate:[AuthGuardService]
      },
    ]
  },
  {
    path: 'login',
    loadChildren: () => import('./components/+login/login.module').then(m => m.LoginModule),
    data: { customLayout: true },
  },
  {
    path: 'g-social',
    //component: GSocialComponent,
    canActivate:[AuthGuardService],
    children: [
      {
        path: '',
        component: GSocialComponent,
        canActivateChild:[AuthGuardService]
      },
      {
        path: 'add',
        component: GSocialFormComponent,
        canActivateChild:[AuthGuardService]
      },
      {
        path: 'edit/:id',
        component: GSocialFormComponent,
        canActivateChild:[AuthGuardService]
      },
      {
        path: 'view/:id',
        component: GSocialViewComponent,
        canActivateChild:[AuthGuardService]
      },
    ]
  },
  {
    path: 'g-umkm',
    component: GUmkmComponent,
    canActivate:[AuthGuardService]
  },
  {
    path: 'g-beasiswa',
    component: GBeasiswaComponent,
    canActivate:[AuthGuardService]
  },
  {
    path: 'g-insurance',
    component: GInsuranceComponent,
    canActivate:[AuthGuardService]
  },
  {
    path: 'g-voucher',
    component: GVoucherComponent,
    canActivate:[AuthGuardService]
  },
  {
    path: 'g-news',
    component: GNewsComponent,
    canActivate:[AuthGuardService]
  },
  {
    path: 'g-training',
    component: GTrainingComponent,
    canActivate:[AuthGuardService]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
