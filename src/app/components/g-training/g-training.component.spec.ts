import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GTrainingComponent } from './g-training.component';

describe('GTrainingComponent', () => {
  let component: GTrainingComponent;
  let fixture: ComponentFixture<GTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
