import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GUmkmComponent } from './g-umkm.component';

describe('GUmkmComponent', () => {
  let component: GUmkmComponent;
  let fixture: ComponentFixture<GUmkmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GUmkmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GUmkmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
