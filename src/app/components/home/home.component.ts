import { Component, AfterViewInit, OnInit } from '@angular/core';

import * as Prism from 'prismjs';
import { Constants } from '../../configs/constants';
import { LayoutService } from 'angular-admin-lte';
import { Cookies } from '../../configs/cookies';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit, OnInit {
  loginTime: any;
  expired: any;

  constructor(
    private cookies: Cookies,
  ) {}

  /**
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {
    Prism.highlightAll();
  }

  ngOnInit(): void {
    this.loginTime = this.cookies.get(Constants.LOGINTIME);
    var date = new Date(this.loginTime);
    date.setHours(date.getHours() + 24);
    this.expired = date;
  }
}
