import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { BtnCellRenderer } from '../../helpers/btnCellRederer.component';
import { Sweetalert } from '../../../configs/sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';
import { GSocialService } from '../../../services/g-social.service';
import { CityService } from '../../../services/city.service';
import { Select2OptionData } from 'ng-select2';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-g-social-form',
  templateUrl: './g-social-form.component.html',
  styleUrls: ['./g-social-form.component.css']
})
export class GSocialFormComponent implements OnInit {

  angForm: FormGroup;
  file: any;
  rowData: any;
  result: any;
  city: Array<Select2OptionData>;

  constructor(
    private formBuilder: FormBuilder,
    private sweetalert: Sweetalert,
    private spinner: NgxSpinnerService,
    private gSocialService : GSocialService,
    private cityService : CityService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.angForm = this.formBuilder.group({
      id: [null],
      title: ["", Validators.required ],
      dateStart: ["", Validators.required ],
      dateEnd: ["", Validators.required ],
      description: ["", Validators.required ],
      city: ["", Validators.required ],
      location: ["", Validators.required ],
      termsCondition: ["", Validators.required ],
      image: [""],
      imageTemp: [""],
      quota: ["", Validators.required ],
      joinLimit: ["", Validators.required ]
    });
    this.getCity();
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    if (id != null) {
      this.gSocialService.view(Number(id)).subscribe(
        result => {
          this.result = result;
          console.log('RESULT EDIT::', this.result);
          this.angForm.controls['id'].setValue(this.result.id);
          this.angForm.controls['title'].setValue(this.result.title);
          this.angForm.controls['description'].setValue(this.result.description);
          this.angForm.controls['city'].setValue(this.result.city);
          this.angForm.controls['location'].setValue(this.result.location);
          this.angForm.controls['quota'].setValue(this.result.quota);
          this.angForm.controls['joinLimit'].setValue(this.result.joinLimit);
          this.angForm.controls['termsCondition'].setValue(this.result.termsCondition);
          this.angForm.controls['dateStart'].setValue(new DatePipe('en-US').transform(this.result.dateStart, 'yyyy-MM-dd'));
          this.angForm.controls['dateEnd'].setValue(new DatePipe('en-US').transform(this.result.dateEnd, 'yyyy-MM-dd'));
          this.angForm.controls['imageTemp'].setValue(this.result.imageBannerName);
        },
        error => {
          this.sweetalert.alertError('Get data failed');
        });
    }
  }

  insert() {
    if (new Date(this.angForm.get('dateStart').value) > new Date(this.angForm.get('dateEnd').value)) {
      this.sweetalert.alertWarning('Start date and end date not valid');
    } else if (this.angForm.dirty && this.angForm.valid) {
      let data = this.getDataForm();
      data['image'] = this.file;
      this.gSocialService.insert(data).subscribe(
        result => {
          this.router.navigate(['/g-social']);
          this.sweetalert.alertSuccess('Save data successfully');
        },
        error => {
          this.sweetalert.alertError('Save data failed');
        }
      );
    } else {
      this.sweetalert.alertWarning('Form not valid');
    }
  }

  update() {
    if (new Date(this.angForm.get('dateStart').value) > new Date(this.angForm.get('dateEnd').value)) {
      this.sweetalert.alertWarning('Start date and end date not valid');
    } else if (this.angForm.dirty && this.angForm.valid) {
      let id = this.angForm.get('id').value;
      let data = this.getDataForm();
      data['updatedBy'] = 'admin';
      console.log('IMAGE::', this.file);
      if (this.file != null) {
        data['image'] = this.file;
      }
      this.gSocialService.update(id, data).subscribe(
        result => {
          this.router.navigate(['/g-social']);
          this.sweetalert.alertSuccess('Update data successfully');
        },
        error => {
          this.sweetalert.alertError('Update data failed');
        }
      );
    } else {
      this.sweetalert.alertWarning('Form not valid');
    }
  }

  getDataForm () {
    return {
      title: this.angForm.get('title').value,
      dateStart: new Date(this.angForm.get('dateStart').value),
      dateEnd: new Date(this.angForm.get('dateEnd').value),
      description: this.angForm.get('description').value,
      city: this.angForm.get('city').value,
      location: this.angForm.get('location').value,
      termsCondition: this.angForm.get('termsCondition').value,
      quota: this.angForm.get('quota').value,
      joinLimit: this.angForm.get('joinLimit').value,
      isShow: this.angForm.get('isShow').value,
    };
  }

  public getCity () {
    this.cityService.getAll().subscribe(
      result => {
        let temp = [];
        result.forEach(function (item) {
          temp.push({id: item['namaKotaKabupaten'], text: item['namaKotaKabupaten']});
        });
        //console.log('CITY', temp);
        this.city = temp;
      })
  }

  onFileChanged(event: any) {
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      this.file = file;
    }
  }

}
