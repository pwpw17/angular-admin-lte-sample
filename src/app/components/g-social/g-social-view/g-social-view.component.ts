import { Component, OnInit } from '@angular/core';
import { GSocialService } from '../../../services/g-social.service';
import { Sweetalert } from '../../../configs/sweetalert';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { BtnCellRenderer } from '../../helpers/btnCellRederer.component';
import { BtnCellViewGsocial } from '../../helpers/btn-cell-view-gsocial';

@Component({
  selector: 'app-g-social-view',
  templateUrl: './g-social-view.component.html',
  styleUrls: ['./g-social-view.component.css']
})
export class GSocialViewComponent implements OnInit {

  result: any;
  angForm: FormGroup;
  rowData: any;
  showMoreDesc: false;
  showMoreTerms: false;

  gridOptions = {
    columnDefs: [
      {
        headerName: 'KTP',
        field: 'option',
        cellRenderer: 'btnCellViewGsocial',
        cellRendererParams: {
          clicked: function(field: any) {}
        },
        minWidth: 80
      },
      //{ headerName: 'Id', field: 'id', filter: false, hide: true, },
      { headerName: 'Nama', field: 'nama', filter: false, minWidth: 150 },
      { headerName: 'No. Telp', field: 'no_telp', filter: false, minWidth: 150 },
      { headerName: 'Kota', field: 'kota', filter: 'agTextColumnFilter', minWidth: 150 },
      { headerName: 'Kecamatan', field: 'kecamatan', hide: true, filter: 'agTextColumnFilter', minWidth: 150},
      { headerName: 'ID Number', field: 'id_number', filter: 'agTextColumnFilter', minWidth: 150},
      { headerName: 'Jenis Kendaraan', field: 'jenis_kendaraan', filter: 'agTextColumnFilter', minWidth: 150},
      { headerName: 'Merk Kendaraan', field: 'merk_kendaraan', filter: false, minWidth: 150 },
      { headerName: 'Tipe Kendaraan', field: 'tipe_kendaraan', filter: false, minWidth: 150 },
      { headerName: 'Tahun Pembuatan', field: 'tahun_pembuatan', filter: false, minWidth: 150 },
      { headerName: 'No. Polisi', field: 'no_polisi', filter: false, minWidth: 150 },
      { headerName: 'Tgl. Lahir', field: 'tgl_lahir', filter: false, minWidth: 150 },
      { headerName: 'Anggota Komunitas', field: 'anggota_komunitas', filter: false, minWidth: 150 },
    ],
    defaultColDef: {
      editable: false,
      sortable: true,
      flex: 1,
      filter: true,
      resizable: true,
    },
    frameworkComponents: {
      btnCellViewGsocial: BtnCellViewGsocial,
    },
    pagination: true,
    paginationPageSize: 10,
    enableColResize: true,
    colResizeDefault: true,
    gridAutoHeight: true,
    animateRows: true,
    suppressCellSelection: true,
    enableCellChangeFlash: false,
    rowData: [],
    onGridReady: function (params) {
      params.api.setRowData(this.rowData);
    },
    onFirstDataRendered(params) {
      params.api.sizeColumnsToFit();
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private gSocialService: GSocialService,
    private sweetalert: Sweetalert,
  ) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.gSocialService.view(Number(id)).subscribe(
      result => {
        console.log('VIEW::', result);
        this.result = result;
      },
      error => {
        this.sweetalert.alertError('Get data failed');
      });

    this.gSocialService.registration(Number(id)).subscribe(
      result => {
        let data = result;
        if (data != null) {
          let temp = [];
          let i = 1;
          data.forEach(function (item) {
            temp.push({
              no: i,
              //id: item['userDetail']['id'],
              nama: item['userDetail']['fullName'],
              no_telp: item['userDetail']['mobilePhone'],
              kota: item['userDetail']['kota'],
              kecamatan: item['userDetail']['kecamatan'],
              id_number: item['userDetail']['idNumber'],
              jenis_kendaraan: item['userDetail']['jenisKendaraan'],
              merk_kendaraan: item['userDetail']['merkKendaraan'],
              tipe_kendaraan: item['userDetail']['tipeKendaraan'],
              tahun_pembuatan: item['userDetail']['tahunPembuatan'],
              no_polisi: item['userDetail']['nopol'],
              tgl_lahir: new Date(item['userDetail']['birthDate']).toLocaleDateString("en-GB"),
              anggota_komunitas: item['userDetail']['anggotaKomunitas']
            });
            i++;
          });
          this.rowData = temp;
          this.gridOptions.rowData = temp;
        }
      },
      error => {
        this.sweetalert.alertError('Get data registration failed');
      });
  }

}
