import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Sweetalert } from '../../configs/sweetalert'
import { GSocialService } from '../../services/g-social.service';
import { forEach } from 'ag-grid-community/dist/lib/utils/array';
import { BtnCellRenderer } from '../helpers/btnCellRederer.component';
import { CityService } from '../../services/city.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Select2OptionData } from 'ng-select2';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-g-social',
  templateUrl: './g-social.component.html',
  styleUrls: ['./g-social.component.css'],
})
@Injectable({
  providedIn: 'root'
})

export class GSocialComponent implements OnInit {

  angForm: FormGroup;
  id: number;
  file: any;
  result: any;
  city: Array<Select2OptionData>;
  rowData: any;

  gridOptions = {
    columnDefs: [
      {
        headerName: '#',
        field: 'option',
        cellRenderer: 'btnCellRenderer',
        cellRendererParams: {
          clicked: function(field: any) {}
        },
        width: 230
      },
      { headerName: 'Id', field: 'id', filter: false, hide: true, },
      { headerName: 'No', field: 'no', filter: false, width: 90 },
      { headerName: 'Title', field: 'title', filter: 'agTextColumnFilter' },
      {
        headerName: 'Start Date',
        field: 'dateStart',
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: function(filterLocalDateAtMidnight, cellValue) {
            var dateAsString = cellValue;
            if (dateAsString == null) {
              return 0;
            }
            var dateParts = dateAsString.split('/');
            var day = Number(dateParts[2]);
            var month = Number(dateParts[1]) - 1;
            var year = Number(dateParts[0]);
            var cellDate = new Date(day, month, year);
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            } else if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            } else {
              return 0;
            }
          }
        }
      },
      {
        headerName: 'End Date',
        field: 'dateEnd',
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: function(filterLocalDateAtMidnight, cellValue) {
            var dateAsString = cellValue;
            if (dateAsString == null) {
              return 0;
            }
            var dateParts = dateAsString.split('/');
            var day = Number(dateParts[2]);
            var month = Number(dateParts[1]) - 1;
            var year = Number(dateParts[0]);
            var cellDate = new Date(day, month, year);
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            } else if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            } else {
              return 0;
            }
          }
        }
      },
      { headerName: 'Description', field: 'desc', hide: true, filter: 'agTextColumnFilter'},
      { headerName: 'City', field: 'city', filter: 'agTextColumnFilter'},
      { headerName: 'Location', field: 'location', filter: 'agTextColumnFilter'},
      { headerName: 'Quota', field: 'quota', filter: false, width: 150},
      { headerName: 'Join Limit', field: 'joinLimit', filter: false, width: 150},
    ],
    defaultColDef: {
      editable: false,
      sortable: true,
      flex: 1,
      //minWidth: 50,
      filter: true,
      //floatingFilter: true,
      resizable: true,
    },
    frameworkComponents: {
      btnCellRenderer: BtnCellRenderer,
    },
    pagination: true,
    paginationPageSize: 10,
    enableColResize: true,
    colResizeDefault: true,
    gridAutoHeight: true,
    animateRows: true,
    suppressCellSelection: true,
    enableCellChangeFlash: false,
    rowData: [],
    onGridReady: function (params) {
      params.api.setRowData(this.rowData);
    },
    onFirstDataRendered(params) {
      params.api.sizeColumnsToFit();
    }
  };

  constructor(
    private formBuilder: FormBuilder,
    private sweetalert: Sweetalert,
    private spinner: NgxSpinnerService,
    private gSocialService : GSocialService,
    private cityService : CityService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.index();
  }

  index () {
    this.getAllData();
  }

  getAllData () {
    this.spinner.show();
    this.gSocialService.getAll().subscribe(
      result => {
        this.result = result;
        console.log('RESULT::', this.result);
        let data = result['content'];
        if (data != null) {
          let temp = []; let i = 1;
          data.forEach(function (item) {
            temp.push({
              no: i,
              id: item['id'],
              title: item['title'],
              dateStart: new Date(item['dateStart']).toLocaleDateString("en-GB"),
              dateEnd: new Date(item['dateEnd']).toLocaleDateString("en-GB"),
              desc: item['description'],
              city: item['city'],
              location: item['location'],
              quota: item['quota'],
              joinLimit: item['joinLimit']});
            i++;
          });
          this.rowData = temp;
          this.gridOptions.rowData = temp;
          this.spinner.hide();
        }
      },
      error => {
        this.sweetalert.alertError('Get all data failed');
      }
    );
  }

  update (data: any) {
    this.router.navigate(['/g-social/edit/' + data['id']]);
  }

  view (data: any) {
    this.router.navigate(['/g-social/view/' + data['id']]);
  }

  delete (params: any) {
    this.sweetalert.alertDelete()
      .then((result) => {
        if (result.value) {
          this.gSocialService.delete(params['id']).subscribe(
            result => {
              this.index();
              this.sweetalert.alertSuccess('Delete successfully');
            });
        }
      });
  }
}
