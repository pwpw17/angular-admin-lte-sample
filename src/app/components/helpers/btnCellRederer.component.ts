import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IAfterGuiAttachedParams } from 'ag-grid-community';
import { GSocialComponent } from '../g-social/g-social.component';

@Component({
  selector: 'btn-cell-renderer',
  template: `
    <button style="margin-right: 5px" class="btn btn-sm btn-info" (click)="btnClickedView(params)">
        <span class="glyphicon glyphicon-eye-open"></span>
    </button>
    <button style="margin-right: 5px" class="btn btn-sm btn-warning" (click)="btnClickedUpdate(params)">
        <span class="glyphicon glyphicon-pencil"></span>
    </button>
    <button class="btn btn-sm btn-danger" (click)="btnClickedDelete(params)">
        <span class="glyphicon glyphicon-trash"></span>
    </button>
  `,
})
export class BtnCellRenderer implements ICellRendererAngularComp, OnDestroy {
  public params: any;

  agInit(params: any): void {
    this.params = params;
  }

  constructor(private comp: GSocialComponent ) { }

  btnClickedView(params) {
    this.comp.view(params['data']);
  }

  btnClickedUpdate(params) {
    this.comp.update(params['data']);
    //this.params.clicked(this.params.value);
  }

  btnClickedDelete(params) {
    this.comp.delete(params['data']);
  }

  ngOnDestroy() {
  }

  afterGuiAttached(params?: IAfterGuiAttachedParams): void {
  }

  refresh(params: any): boolean {
    return false;
  }
}
