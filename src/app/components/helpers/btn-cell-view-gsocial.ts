import { ICellRendererAngularComp } from 'ag-grid-angular';
import { GSocialViewComponent } from '../g-social/g-social-view/g-social-view.component';
import { IAfterGuiAttachedParams } from 'ag-grid-community';
import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'btn-cell-view-gsocial',
  template: `
    <button style="margin-right: 5px" class="btn btn-sm btn-info" (click)="btnClickedView(params)">
        <span class="glyphicon glyphicon-credit-card"></span>
    </button>
  `,
})
export class BtnCellViewGsocial implements ICellRendererAngularComp, OnDestroy {
  public params: any;

  agInit(params: any): void {
    this.params = params;
  }

  constructor(private comp: GSocialViewComponent ) { }

  btnClickedView(params) {
    //this.comp.view(params['data']);
  }

  ngOnDestroy() {
  }

  afterGuiAttached(params?: IAfterGuiAttachedParams): void {
  }

  refresh(params: any): boolean {
    return false;
  }
}
