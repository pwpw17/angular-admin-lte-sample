import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GInsuranceComponent } from './g-insurance.component';

describe('GInsuranceComponent', () => {
  let component: GInsuranceComponent;
  let fixture: ComponentFixture<GInsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GInsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
