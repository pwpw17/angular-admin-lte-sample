import { Component, Injectable, Input, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Cookies } from '../../configs/cookies';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { error } from 'ng-packagr/lib/utils/log';
import { LayoutModule } from 'angular-admin-lte';
import { Constants } from '../../configs/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [Cookies],
})
@Injectable({
  providedIn: 'root'
})

export class LoginComponent implements OnInit {

  @Input() userLogin = { username: '', password: ''};

  angForm: FormGroup;
  isLoginFailed = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private cookies: Cookies,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.angForm = this.formBuilder.group({
      username: ['', Validators.required ],
      password: ['', Validators.required ],
    });
  }

  // login() {
  //   let user = {
  //     username: this.angForm.get('username').value,
  //     password: this.angForm.get('password').value,
  //   };
  //   this.authService.login(user).subscribe(
  //     result => {
  //       console.log('LoginComponent', result);
  //       this.cookies.put(Constants.AUTHORIZATION, result['Authorization']);
  //       this.cookies.put(Constants.ROLES, result['Roles']);
  //       this.cookies.put(Constants.LOGINTIME, new Date().toString());
  //       this.router.navigate([''])
  //     },
  //     error => {
  //       console.log('LoginComponent', error);
  //       this.isLoginFailed = true;
  //       setTimeout(() => {
  //         this.isLoginFailed = false;
  //       }, 1000);
  //     }
  //   );
  // }

  login() {
    this.cookies.put(Constants.AUTHORIZATION,'dummy');
    this.cookies.put(Constants.ROLES, 'ROLE_ADMIN');
    this.cookies.put(Constants.LOGINTIME, new Date().toString());
    this.router.navigate([''])
  }

}
