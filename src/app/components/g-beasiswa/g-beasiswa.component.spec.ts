import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GBeasiswaComponent } from './g-beasiswa.component';

describe('GBeasiswaComponent', () => {
  let component: GBeasiswaComponent;
  let fixture: ComponentFixture<GBeasiswaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GBeasiswaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GBeasiswaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
