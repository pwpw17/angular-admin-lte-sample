import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GVoucherComponent } from './g-voucher.component';

describe('GVoucherComponent', () => {
  let component: GVoucherComponent;
  let fixture: ComponentFixture<GVoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GVoucherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GVoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
