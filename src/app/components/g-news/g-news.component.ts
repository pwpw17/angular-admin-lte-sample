import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-g-news',
  templateUrl: './g-news.component.html',
  styleUrls: ['./g-news.component.css']
})
export class GNewsComponent implements OnInit {

  angForm: FormGroup;
  showTable = true;
  showForm = true;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.showTable = true;
    this.showForm = false;
  }

  create() {
    this.showTable = false;
    this.showForm = true;
    this.form();
  }

  edit() {

  }

  delete () {

  }

  back () {
    this.showTable = true;
    this.showForm = false;
  }

  form() {
    this.angForm = this.formBuilder.group({
      title: ['', Validators.required ],
      date: ['', Validators.required ],
      description: ['', Validators.required ],
      location: ['', Validators.required ],
      termsCondition: ['', Validators.required ],
      file: ['', Validators.required ]
    });
  }

}
