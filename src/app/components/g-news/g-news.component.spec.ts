import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GNewsComponent } from './g-news.component';

describe('GNewsComponent', () => {
  let component: GNewsComponent;
  let fixture: ComponentFixture<GNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
