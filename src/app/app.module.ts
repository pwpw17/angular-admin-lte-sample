import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { adminLteConf } from './admin-lte.conf';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { LayoutModule } from 'angular-admin-lte';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';
import { HttpClientModule } from '@angular/common/http';
import { GUmkmComponent } from './components/g-umkm/g-umkm.component';
import { GSocialComponent } from './components/g-social/g-social.component';
import { GSocialFormComponent } from './components/g-social/g-social-form/g-social-form.component';
import { GVoucherComponent } from './components/g-voucher/g-voucher.component';
import { GInsuranceComponent } from './components/g-insurance/g-insurance.component';
import { GTrainingComponent } from './components/g-training/g-training.component';
import { GBeasiswaComponent } from './components/g-beasiswa/g-beasiswa.component';
import { GNewsComponent } from './components/g-news/g-news.component';
import { AgGridModule } from 'ag-grid-angular';
import { BtnCellRenderer } from './components/helpers/btnCellRederer.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgSelect2Module } from 'ng-select2';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GSocialViewComponent } from './components/g-social/g-social-view/g-social-view.component';
import { ControlMessageComponent } from './components/helpers/control-message.component';
import { BtnCellViewGsocial } from './components/helpers/btn-cell-view-gsocial';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    LayoutModule.forRoot(adminLteConf),
    LoadingPageModule,
    MaterialBarModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([
      BtnCellRenderer,
      BtnCellViewGsocial
    ]),
    NgxSpinnerModule,
    NgSelect2Module,
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    GUmkmComponent,
    GSocialComponent,
    GSocialFormComponent,
    GVoucherComponent,
    GInsuranceComponent,
    GTrainingComponent,
    GBeasiswaComponent,
    GNewsComponent,
    BtnCellRenderer,
    BtnCellViewGsocial,
    GSocialViewComponent,
    ControlMessageComponent
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
