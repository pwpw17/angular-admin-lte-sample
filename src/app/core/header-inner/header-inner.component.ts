import { Component, OnInit } from '@angular/core';
import { Cookies } from '../../configs/cookies';
import { Router } from '@angular/router';
import { Constants } from '../../configs/constants';
@Component({
  selector: 'app-header-inner',
  templateUrl: './header-inner.component.html'
})
export class HeaderInnerComponent {

  constructor(
    private router: Router,
    private cookies: Cookies
  ) { }

  logout () {
    this.cookies.put(Constants.AUTHORIZATION, null);
    this.cookies.put(Constants.ROLES, null);
    this.cookies.put(Constants.LOGINTIME, null);
    this.router.navigate(['/login'])
  }

}
