export const adminLteConf = {
  skin: 'blue',
  // isSidebarLeftCollapsed: false,
  // isSidebarLeftExpandOnOver: false,
  // isSidebarLeftMouseOver: false,
  // isSidebarLeftMini: true,
  // sidebarRightSkin: 'dark',
  // isSidebarRightCollapsed: true,
  // isSidebarRightOverContent: true,
  // layout: 'normal',
  sidebarLeftMenu: [
    {
      label: 'MAIN NAVIGATION', separator: true
    },
    // {label: 'Start', route: '/', iconClasses: 'fa fa-road', pullRights: [{text: 'New', classes: 'label pull-right bg-green'}]},
    // {label: 'Layout', iconClasses: 'fa fa-th-list', children: [
    //     {label: 'Configuration', route: 'layout/configuration'},
    //     {label: 'Custom', route: 'layout/custom'},
    //     {label: 'Header', route: 'layout/header'},
    //     {label: 'Sidebar Left', route: 'layout/sidebar-left'},
    //     {label: 'Sidebar Right', route: 'layout/sidebar-right'},
    //     {label: 'Content', route: 'layout/content'}
    //   ]},
    // {label: 'COMPONENTS', separator: true},
    // {label: 'Accordion', route: 'accordion', iconClasses: 'fa fa-tasks'},
    // {label: 'Alert', route: 'alert', iconClasses: 'fa fa-exclamation-triangle'},
    // {label: 'Boxs', iconClasses: 'fa fa-files-o', children: [
    //     {label: 'Default Box', route: 'boxs/box'},
    //     {label: 'Info Box', route: 'boxs/info-box'},
    //     {label: 'Small Box', route: 'boxs/small-box'}
    //   ]},
    // {label: 'Dropdown', route: 'dropdown', iconClasses: 'fa fa-arrows-v'},
    {
      label: 'Dasboard', iconClasses: 'fa fa-dashboard',  route: '/'
    },
    {
      label: 'G-Social', iconClasses: 'fa fa-globe',  route: '/g-social'
    },
    {
      label: 'G-UMKM', iconClasses: 'fa fa-building',  route: '/g-umkm'
    },
    {
      label: 'G-Voucher', iconClasses: 'fa fa-ticket',  route: '/g-voucher'
    },
    {
      label: 'G-Insurance', iconClasses: 'fa fa-money',  route: '/g-insurance'
    },
    {
      label: 'G-Training', iconClasses: 'fa fa-child',  route: '/g-training'
    },
    {
      label: 'G-Beasiswa', iconClasses: 'fa fa-graduation-cap',  route: '/g-beasiswa'
    },
    {
      label: 'G-News', iconClasses: 'fa fa-newspaper-o',  route: '/g-news'
    },

  ]
};
