import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class Params {

  getParams(column: string, qlue: string, sortBy: string, pageSize: string, sortType: string) {
    return new HttpParams()
      .set('column', column != '' ? column : 'title')
      .set('qlue', qlue != '' ? qlue : '')
      .set('sortBy', sortBy != '' ? sortBy : 'createdAt')
      .set('pageSize', pageSize != '' ? pageSize : '10000')
      .set('sortType', sortType != '' ? sortType : 'desc')
  }

}
