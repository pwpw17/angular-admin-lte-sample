import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class Constants {
  static AUTHORIZATION = 'Authorization';
  static ROLES = 'Roles';
  static LOGINTIME = 'LoginTime';
}
