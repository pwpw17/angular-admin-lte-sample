import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})

export class Cookies {
  constructor(
    private cookieService: CookieService
  ) {}

  put(key: string, value: string) {
    this.cookieService.set(key, value);
  }

  get(key: string) {
    return this.cookieService.get(key);
  }

}
