import { Injectable } from '@angular/core';
import Swal from "sweetalert2";

@Injectable({
  providedIn: 'root'
})

export class Sweetalert {

  alertSuccess (message: string) {
    Swal.fire({
      title: message, icon: "success", showConfirmButton: false, timer: 1500,
    });
  }

  alertError (message: string) {
    Swal.fire({
      title: message, icon: "error", showConfirmButton: false, timer: 1500,
    });
  }

  alertWarning (message: string) {
    Swal.fire({
      title: message, icon: "warning", showConfirmButton: false, timer: 1500,
    });
  }

  alertDelete () {
    return Swal.fire({
      title: 'Are you sure delete this item?',
      confirmButtonColor: '#db4a3c',
      showConfirmButton: true,
      showCancelButton: true,
    })
  }

}
