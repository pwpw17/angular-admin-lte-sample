import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class Headers {

  getHeader(authorization: string) {
    return new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      // 'User-Agent': 'PostmanRuntime/7.26.2',
      // 'Accept': '*/*',
      // 'Accept-Encoding': 'gzip, deflate, br',
      // 'Connection': 'keep-alive',
      'Authorization': 'Bearer ' + authorization,
    })
  }

  getHeaderPost() {
    return  new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

}
